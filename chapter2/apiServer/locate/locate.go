package locate

import (
	"log"
	"os"
	"strconv"
	"time"

	"../../../src/lib/rabbitmq"
)

func Locate(name string) string {
	q := rabbitmq.New(os.Getenv("RABBITMQ_SERVER"))
	log.Println("Publish dataServers:", name)
	q.Publish("dataServers", name)
	c := q.Consume()
	go func() {
		time.Sleep(time.Second)
		q.Close()
	}()
	msg := <-c
	log.Println("Consume:", string(msg.Body))
	s, _ := strconv.Unquote(string(msg.Body))
	return s
}

func Exist(name string) bool {
	return Locate(name) != ""
}
