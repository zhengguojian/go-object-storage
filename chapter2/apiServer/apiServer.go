package main

import (
	"log"
	"net/http"
	"os"

	"./heartbeat"
	"./locate"
	"./objects"
)

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	log.Println("apiServer in main")

	os.Setenv("RABBITMQ_SERVER", "amqp://guest:guest@localhost:5672/")
	os.Setenv("LISTEN_ADDRESS", os.Args[1])

	go heartbeat.ListenHeartbeat()

	http.HandleFunc("/objects/", objects.Handler)
	http.HandleFunc("/locate/", locate.Handler)

	addr := os.Getenv("LISTEN_ADDRESS")
	log.Println("addr:", addr)

	log.Fatal(http.ListenAndServe(addr, nil))
}
