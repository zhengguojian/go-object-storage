#!/bin/bash

首先介绍我们的测试环境，从本章开始一直到本书结束，我们的测试环境都不再发
生变化，始终包括 6 个数据服务节点和 2 个接口服务节点，共 8 个节点。为了方便测试，
这 8 个节点其实都运行在同一台服务器上，只是绑定了 8 个不同的地址加以区分。
6 个数据服务节点地址分别是 127.0.0.1:12345、127.0.0.2:12345、127.0.0.3:12345、
127.0.0.4:12345、127.0.0.5:12345、127.0.0.6:12345。2 个接口服务节点地址是 10.29.2.1:12345和 10.29.2.2:12345。
在同一台服务器上绑定多个地址的命令如下：(记得sudo)
ifconfig ens33:1 127.0.1.1/16 
ifconfig ens33:2 127.0.1.2/16 
ifconfig ens33:3 127.0.1.3/16 
ifconfig ens33:4 127.0.1.4/16 
ifconfig ens33:5 127.0.1.5/16 
ifconfig ens33:6 127.0.1.6/16 
ifconfig ens33:7 127.0.2.1/16 
ifconfig ens33:8 127.0.2.2/16

go get "github.com/streadway/amqp"

为了让我们的节点能够建立消息队列，我们还需要一台 RabbitMQ 服务器，在其上安装 rabbitmq-server。
sudo apt-get install rabbitmq-server

for i in `seq 1 6`; do mkdir -p /tmp/$i/objects; done

sudo go run dataServer/dataServer.go 127.0.1.1:12345 /tmp/1
sudo go run dataServer/dataServer.go 127.0.1.2:12345 /tmp/2
sudo go run dataServer/dataServer.go 127.0.1.3:12345 /tmp/3
sudo go run dataServer/dataServer.go 127.0.1.4:12345 /tmp/4
sudo go run dataServer/dataServer.go 127.0.1.5:12345 /tmp/5
sudo go run dataServer/dataServer.go 127.0.1.6:12345 /tmp/6

sudo go run apiServer/apiServer.go 127.0.2.1:12345
sudo go run apiServer/apiServer.go 127.0.2.2:12345

ps -ef | grep -v grep | grep dataServer  | awk '{print $2}' | sudo xargs kill -9
ps -ef | grep -v grep | grep apiServer  | awk '{print $2}' | sudo xargs kill -9



curl -v 127.0.2.2:12345/objects/test2 -XPUT -d"this is object test2"

curl 127.0.2.2:12345/locate/test2

curl 10.29.2.1:12345/objects/test2
