package main

import (
	"log"
	"net/http"
	"os"

	"./heartbeat"
	"./locate"
	"./objects"
)

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	log.Println("dataServer in main")

	os.Setenv("RABBITMQ_SERVER", "amqp://guest:guest@localhost:5672/")
	os.Setenv("LISTEN_ADDRESS", os.Args[1])
	os.Setenv("STORAGE_ROOT", os.Args[2])

	addr := os.Getenv("LISTEN_ADDRESS")
	log.Println("addr:", addr)

	go heartbeat.StartHeartbeat()
	go locate.StartLocate()

	http.HandleFunc("/objects/", objects.Handler)
	log.Fatal(http.ListenAndServe(addr, nil))
}
