package main

import (
	"log"
	"net/http"
	"os"

	"./heartbeat"
	"./locate"
	"./objects"
	"./versions"
)

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	log.Println("apiServer in main")

	os.Setenv("LISTEN_ADDRESS", os.Args[1])
	os.Setenv("RABBITMQ_SERVER", "amqp://guest:guest@localhost:5672/")
	os.Setenv("ES_SERVER", "127.0.0.1:9200")

	go heartbeat.ListenHeartbeat()

	http.HandleFunc("/objects/", objects.Handler)
	http.HandleFunc("/locate/", locate.Handler)
	http.HandleFunc("/versions/", versions.Handler)

	addr := os.Getenv("LISTEN_ADDRESS")
	log.Println("addr:", addr)

	log.Fatal(http.ListenAndServe(addr, nil))
}
