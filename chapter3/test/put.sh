#!/bin/bash

sudo apt install elasticsearch

curl 127.0.0.1:9200/metadata -XPUT -H 'content-Type:application/json' -d'{"mappings":{"objects": 
{"properties": {"name":{"type": "string","index":"not-analyzed"},"version": 
{"type":"integer"}, "size":{"type":"integer"}, "hash":{"type":"string"}}}}}'

启动程序同chapter2

curl -v 127.0.2.2:12345/objects/test3 -XPUT -d"this is object test3" -H "Digest: SHA-256=GYqqAdFPt+CScnUDc0/Gcu3kwcWmOADKNYpiZtdbgsM="

curl -v 127.0.2.1:12345/objects/test3 -XPUT -d"this is object test3 version 2" -H "Digest: SHA-256=cAPvsxZe1PR54zIESQy0BaxC1pYJIvaHSF3qEOZYYIo="

curl 127.0.2.1:12345/locate/GYqqAdFPt+CScnUDc0%2FGcu3kwcWmOADKNYpiZtdbgsM=

curl 127.0.2.1:12345/locate/cAPvsxZe1PR54zIESQy0BaxC1pYJIvaHSF3qEOZYYIo=

curl 127.0.2.1:12345/versions/test3

