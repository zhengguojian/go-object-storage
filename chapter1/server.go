package main

import (
	"log"
	"net/http"
	"os"

	"./objects"
)

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	log.Println("apiServer in main")

	os.Setenv("LISTEN_ADDRESS", ":12345")
	os.Setenv("STORAGE_ROOT", "/tmp")

	http.HandleFunc("/objects/", objects.Handler)
	log.Fatal(http.ListenAndServe(os.Getenv("LISTEN_ADDRESS"), nil))
}
